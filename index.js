function verExperiencia() {
  var element = document.getElementById('experience');
  element.classList.add("active");
  document.getElementById('studies').classList.remove("active");
}

function verEstudios() {
  var element = document.getElementById('studies');
  element.classList.add("active");
  document.getElementById('experience').classList.remove("active");
}

function addData(element_id){
    // 1. Recibir y almacenar valor 
    var input = prompt ('Insertar texto');
    // 2. Reemplazar el texto de spam por el valor introduido.
   document.getElementById(element_id).innerText = input;
}

function addExperience(){
    var expName = prompt('Nombre de la experiencia');
    var expStartDate = prompt('¿Cuando empezaste?');
    var expEndDate = prompt('¿Cuando finalizaste?');
    var container = document.getElementById('experience');

    container.innerHTML+= createSentece(expStartDate, expEndDate, expName);
}

function addStudies(){
    var expStudy = prompt("Nombre estudi¡os");
    var expStartDate = prompt('¿Cuando empezaste?');
    var expEndDate = prompt('¿Cuando finalizaste?');
    var container = document.getElementById('studies');

    container.innerHTML+= createSentece(expStartDate, expEndDate, expStudy);
}

function createSentece(startDate, endDate, name){
   return "<p><strong>"+startDate + " - " + endDate +"</strong> - "+ name + "</p>";
}

function addLinks(){
    var nameLink = prompt("Nombre red social");
    var link = prompt("Introduce link");
    var container = document.getElementById("link");

  container.innerHTML+= "<li><a class='social-button' href="+link + " title=" +nameLink + " >"+ nameLink + "</a></li>"
}